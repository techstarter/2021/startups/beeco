# 2. Embedded solution

###### Embedded Programming List 

1. Temperature and Humidity Monitoring
2. Contactless Temperature and Humidity Monitoring
3. Weigh bridge measurment
4. Esp32 connectivity tests
5. Ota 
6. Lora testing 


## 2.1 Objectives

<Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken>


## 2.3 Steps taken

![alt text]()

#### Components used 


## 2.4 Testing & Problems

### Weight bridge with Arduino Uno 

```
//#include <LiquidCrystal.h>
//LiquidCrystal lcd(8, 9, 10, 11, 12, 13);

#define DT A1
#define SCK 13
#define sw 2

long sample=0;
float val=0;
long count=0;

unsigned long readCount(void)
{
  unsigned long Count;
  unsigned char i;
  pinMode(DT, OUTPUT);
  digitalWrite(DT,HIGH);
  digitalWrite(SCK,LOW);
  Count=0;
  pinMode(DT, INPUT);
  while(digitalRead(DT));
  for (i=0;i<24;i++)
  {
    digitalWrite(SCK,HIGH);
    Count=Count<<1;
    digitalWrite(SCK,LOW);
    if(digitalRead(DT)) 
    Count++;
  }
  digitalWrite(SCK,HIGH);
  Count=Count^0x800000;
  digitalWrite(SCK,LOW);
  return(Count);
}

void setup()
{
  Serial.begin(9600);
  pinMode(SCK, OUTPUT);
  pinMode(sw, INPUT_PULLUP);
////  lcd.begin(16, 2);
//  lcd.print("    Weight ");
//  lcd.setCursor(0,1);
//  lcd.print(" Measurement ");
//  delay(1000);
//  lcd.clear();
  calibrate();
}

void loop()
{
  count= readCount();
  int w=(((count-sample)/val)-2*((count-sample)/val));
  Serial.print("weight:");
  Serial.print((int)w);
  Serial.println("g");
  delay(1000);
//  lcd.setCursor(0,0);
//  lcd.print("Weight            ");
//  lcd.setCursor(0,1);
//  lcd.print(w);
//  lcd.print("g             ");

  if(digitalRead(sw)==0)
  {
    val=0;
    sample=0;
    w=0;
    count=0;
    calibrate();
  }
}

void calibrate()
{
//  lcd.clear();
//  lcd.print("Calibrating...");
Serial.print("Calibrating... plz wait");
//  lcd.setCursor(0,1);
//  lcd.print("Please Wait...");
  for(int i=0;i<100;i++)
  {
    count=readCount();
    sample+=count;
    Serial.println(count);
  }
  sample/=100;
  Serial.print("Avg:");
  Serial.println(sample);
//  lcd.clear();
  Serial.print("Put 100g & wait");
  delay(2000);
  count=0;
  while(count<100)
  {
    count=readCount();
    count=sample-count;
    Serial.println(count);
  }
//  lcd.clear();
  Serial.print("Please Wait....");
  delay(2000);
  for(int i=0;i<100;i++)
  {
    count=readCount();
    val+=sample-count;
    Serial.println(sample-count);
  }
  val=val/100.0;
  val=val/100.0;        // put here your calibrating weight
//  lcd.clear();
  Serial.print("Calibration done!");
  delay(1000);
}

```


```


```

## 2.5 Proof of work

< Add video and/or photos of your tests and proof it works!>

## 2.6 Files & Code

<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 2.7 References & Credits

<add all used references & give credits to those u used code and libraries from>

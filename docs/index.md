# 1. Project Title: BeeCO

                                ### Image of Architecture Here

## Introduction 
Bees are very important for terrestrial ecosystems and, above all, for the subsistence of many crops, due to their ability to pollinate flowers. Currently, the honey bee populations are decreasing due to colony collapse disorder (CCD). 

The reasons for CCD are not fully known, and as a result, it is essential to obtain all possible information on the environmental conditions surrounding the beehives. 


## Overview & Features

BeeCo is a low-cost, fully scalable, easily deployable system with regard to the number and types of sensors and the number of hives and their geographical distribution. 

BeeCo saves the data in each of the levels if there are failures in communication.

In addition, the nodes include a backup battery, which allows for further data acquisition and storage in the event of a power outage. Unlike other systems that monitor a single point of a hive, the system we present monitors and stores the temperature and relative humidity of the beehive in three different spots.
 
Additionally, the hive is continuously weighed on a weighing scale. Real-time weight measurement is an innovation in wireless beehive—monitoring systems. We designed an adaptation board to facilitate the connection of the sensors to the node. 
 
Through the Internet, researchers and beekeepers can access the cloud data server to find out the condition of their hives in real time


Beehive monitoring allows us to monitor different parameters, such as the temperature and humidity levels inside the beehives, as well as the weight, sounds, and gases produced, which can generate important information. 

For example, these data can inform us on whether the beehives are swarming based on the temperature, whether any action is required from the beekeeper, whether the bees are affected by any disease, or even whether the hives are moving. This last application is very useful in areas where beehives can be stolen.

## Demo / Proof of work

<iframe width="560" height="315" src="https://www.youtube.com/embed/zlpPV9lR5JI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

 dark             |   Ocean
:-------------------------:|:-------------------------:
![](img/bee1.png)  |  ![](img/bee2.png)

 dark             |   Ocean
:-------------------------:|:-------------------------:
![](img/bee3.png) |    ![](img/bee3.png) 

 dark             |   Ocean
:-------------------------:|:-------------------------:
![](img/bee4.png) |     ![](img/bee5.png) 

## Deliverables

< The deliverables of the project, so we know when it is complete>
- Project poster
- Proof of concept working
- Product pitch deck

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR47oGOAGTzFclFwp2vBAq2DsclLGmDmKalnIb1Ysf4-WD5h8FDwcZumBY6ETQrA1SvYPfwGiBDD8QE/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Project landing page/website (optional)


## the team & contact

1. Theo Boomsma 
2. John Vd Zijden 
3. Julie Sundar 
4. Rawien Sidhoe
5. Ashton Brielle 

# Contact
+597 8503373 


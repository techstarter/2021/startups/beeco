# Notes


###### Embedded Programming List / Research 

1. Temperature monitoring (alerts when temp is too high or too low depending on user settings)
2. Water/Feed Level alert (alerts when low)
3. Air Quality Control (monitors humidity, ammonia, and CO2)
4. Nesting Box Egg detector (detects when an egg is laid in a box)
5. Hen laying tracker (tracks which hen laid an egg)
6. Chicken Tracking (alerts when chickens get outside a perimeter such as into a garden or any other area)
7. Automatic Door (opens/closes on timer or by light)
8. Automatic Heating (turns on a "safe" heater when temp drops below a certain point)
9. Automatic Cooling (fan kicks on when temp gets above a certain point)
10. Automatic Lighting (controls coop lighting/provides more light when daylight lessens for optimal laying)
11. Auto Water Defroster (turns on to prevent water from freezing in low temps)
12. Video Camera Monitoring
13. Alerts when preditor is in the area maybe by sound 
14. Solar Powered?
